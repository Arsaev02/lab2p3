import React, {useState, useEffect} from 'react';
import {
  IonRow,
  IonCol,
  IonImg
} from '@ionic/react';
import './CategorySelector.css';


interface CategorySelectorProperties {
  onItemClicked: (id: string) => void
}

const CategorySelector: React.FC<CategorySelectorProperties> = ({onItemClicked}: CategorySelectorProperties) => {

  const apiUrl = 'http://127.0.0.1:8000/';
  const categorisApi = apiUrl + 'api/categories/';
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetch(`${categorisApi}`).then(async response => {
      let category = (await response.json()).map((cat: any) => {
        return {...cat, date: new Date(cat.date).toLocaleDateString()}
      });
      setCategories(category);
    });
  }, []);

  return (
    <div className="container-categories">
      {
        categories.map((vtype: any) => {
          const path = vtype.image.url.split('/');
          const nameImage = path[path.length - 1];
          return (
            <IonRow className="cars" key={vtype.id} onClick={() => onItemClicked(vtype.id)}>
              <IonCol size="2">
                <IonImg className="image-categories" src={apiUrl + 'media/img/' + nameImage}/>
              </IonCol>
              <IonCol className="text-categories" size="10">{vtype.name}</IonCol>
            </IonRow>
          );
        })
      }
    </div>
  );
};
export default CategorySelector;