import {
  IonButtons, IonCard, IonCardContent, IonCardHeader,
  IonContent,
  IonHeader, IonImg, IonItem, IonList,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonGrid,
  IonCol,
  IonRow,
  IonButton,
  IonModal,
  IonRouterOutlet
} from '@ionic/react';
import React, { useEffect, useState } from 'react';
import './PlaceOrder.css';

import NewOrder from "../../components/NewOrder";
import { RouteComponentProps, Route, Redirect, useHistory } from "react-router";
import { YMaps, Map } from "react-yandex-maps";
import { IonReactRouter } from '@ionic/react-router';

const apiUrl = 'http://127.0.0.1:8000/api/jobs';

interface PlaceOrderPageProperties extends RouteComponentProps<{
  categoryId: string;
  history:any
}> { }

const PlaceOrderPage: React.FC<PlaceOrderPageProperties> = ({ match}: PlaceOrderPageProperties) => {
  let history = useHistory()
  return (

    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Заказы</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <div className="z">
          <IonButton color="light" className="back-button" onClick={()=> {
                history.goBack()
              }}
           >&lt;</IonButton>
          
        </div>

        <div className="map-wrapper">
          <YMaps>
            <Map width="100%" height="100%" defaultState={{ center: [43.3219485, 45.674585], zoom: 16 }} />
          </YMaps>
        </div>
              
        <IonModal isOpen={true}>
          <NewOrder idCategory={match.params.categoryId} />
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default PlaceOrderPage;
