from rest_framework import serializers
<<<<<<< HEAD

from core.serializers import AddressSerializer
from drivers.serializers import VehicleTypeSerializer
from core.serializers import ImageSerializer
from .models import Job, JobImage, Category, ReviewImage, Order, \
    OrderCancellation, Offer, Review, CategoryVehicleType


class CategorySerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = Category
        fields = '__all__'


class CreateJobSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    dropoff = AddressSerializer()

    class Meta:
        model = Job
        fields = '__all__'


class JobSerializerPopulated(serializers.ModelSerializer):

    address = AddressSerializer()
    dropoff = AddressSerializer()
    category = CategorySerializer()
    vehicle_type = VehicleTypeSerializer()

    class Meta:
        model = Job
        fields = '__all__'


class JobImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobImage
        fields = '__all__'
=======
from .models import Job, JobImage, Category, CategoryVehicleType, Offer, Review, ReviewImage, Order, OrderCancellation, JobStatus, OrderStatus, UserReason, DriverReason


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '_all_'


class JobImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobImage
        fields = '_all_'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '_all_'


class CategoryVehicleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryVehicleType
        fields = '_all_'
>>>>>>> origin/master


class OfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
<<<<<<< HEAD
        fields = '__all__'
=======
        fields = '_all_'
>>>>>>> origin/master


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
<<<<<<< HEAD
        fields = '__all__'
=======
        fields = '_all_'
>>>>>>> origin/master


class ReviewImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewImage
<<<<<<< HEAD
        fields = '__all__'
=======
        fields = '_all_'
>>>>>>> origin/master


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
<<<<<<< HEAD
        fields = '__all__'
=======
        fields = '_all_'
>>>>>>> origin/master


class OrderCancellationSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderCancellation
<<<<<<< HEAD
        fields = '__all__'


class CategoryVehicleTypeSerializer(serializers.ModelSerializer):
    vehicle_type = VehicleTypeSerializer()
    category = CategorySerializer()

    class Meta:
        model = CategoryVehicleType
        fields = '__all__'
=======
        fields = '_all_'
>>>>>>> origin/master
