from django.urls import include, path
from rest_framework import routers
<<<<<<< HEAD
from .views import JobViewSet, JobImageViewSet, CategoryViewSet, ReviewImageViewSet, \
    OrderViewSet, OrderCancellationViewSet, OfferViewSet, ReviewViewSet, CategoryVehicleTypeViewSet,\
    JobDetailViewSet

router = routers.DefaultRouter()
router.register(r'job-images', JobImageViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'review-images', ReviewImageViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'order-cancellations', OrderCancellationViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'category-vehicle-type', CategoryVehicleTypeViewSet)

urlpatterns = [
    path('jobs/', JobViewSet.as_view(), name='jobs'),
    path(r'jobs/<int:pk>', JobDetailViewSet, name='job_details'),
=======
from .views import JobViewSet, JobImageViewSet, CategoryViewSet, CategoryVehicleTypeViewSet,  OfferViewSet, ReviewViewSet, ReviewImageViewSet, OrderViewSet, OrderCancellationViewSet


router = routers.DefaultRouter()
router.register(r'jobs', JobViewSet)
router.register(r'jobs-image', JobImageViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'categories-vehicle-type', CategoryVehicleTypeViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'reviews-image', ReviewImageViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'orders-cancellation', OrderCancellationViewSet)

urlpatterns = [
>>>>>>> origin/master
    path('', include(router.urls)),
]
