from django.urls import include, path
from rest_framework import routers
<<<<<<< HEAD
from .views import DriversViewSet, VehicleViewSet, VehicleBrandViewSet, VehicleModelViewSet, VehicleImageViewSet, \
    VehicleTypeViewSet
=======
from .views import DriversViewSet, VehicleViewSet, VehicleBrandViewSet, VehicleModelViewSet, VehicleImageViewSet, VehicleTypeViewSet

>>>>>>> origin/master

router = routers.DefaultRouter()
router.register(r'drivers', DriversViewSet)
router.register(r'vehicles', VehicleViewSet)
router.register(r'vehicles-brand', VehicleBrandViewSet)
router.register(r'vehicles-model', VehicleModelViewSet)
router.register(r'vehicles-image', VehicleImageViewSet)
router.register(r'vehicles-type', VehicleTypeViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
