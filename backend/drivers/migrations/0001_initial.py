<<<<<<< HEAD
# Generated by Django 3.0.6 on 2020-05-20 15:50
=======
# Generated by Django 3.0.4 on 2020-04-27 23:53
>>>>>>> origin/master

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
<<<<<<< HEAD
                ('bio', models.CharField(blank=True, max_length=1024, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated_at')),
=======
                ('bio', models.CharField(blank=True, max_length=1024)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='update_at')),
>>>>>>> origin/master
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('registration_plate', models.CharField(max_length=50)),
<<<<<<< HEAD
                ('status', models.IntegerField(choices=[('active', 0), ('inactive', 1), ('broken', 2)])),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated_at')),
=======
                ('status', models.IntegerField(choices=[(0, 'active'), (1, 'inactive'), (2, 'broken')])),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='update_at')),
>>>>>>> origin/master
                ('driver', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='drivers.Driver')),
            ],
        ),
        migrations.CreateModel(
            name='VehicleBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=254)),
<<<<<<< HEAD
                ('description', models.CharField(blank=True, max_length=245, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated_at')),
=======
                ('description', models.CharField(blank=True, max_length=245)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='update_at')),
>>>>>>> origin/master
            ],
        ),
        migrations.CreateModel(
            name='VehicleType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
<<<<<<< HEAD
                ('description', models.CharField(blank=True, max_length=245, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated_at')),
=======
                ('description', models.CharField(blank=True, max_length=245)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='update_at')),
>>>>>>> origin/master
                ('image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.Image')),
            ],
        ),
        migrations.CreateModel(
            name='VehicleModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=254)),
                ('volume', models.DecimalField(decimal_places=5, max_digits=20)),
                ('payload', models.DecimalField(decimal_places=5, max_digits=20)),
<<<<<<< HEAD
                ('description', models.CharField(blank=True, max_length=254, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated_at')),
=======
                ('description', models.CharField(blank=True, max_length=254)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='update_at')),
>>>>>>> origin/master
                ('brand', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='drivers.VehicleBrand')),
                ('type', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='drivers.VehicleType')),
            ],
        ),
        migrations.CreateModel(
            name='VehicleImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created_at')),
<<<<<<< HEAD
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated_at')),
=======
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='update_at')),
>>>>>>> origin/master
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Image')),
                ('vehicle', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='drivers.Vehicle')),
            ],
        ),
        migrations.AddField(
            model_name='vehicle',
            name='model',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='drivers.VehicleModel'),
        ),
    ]
