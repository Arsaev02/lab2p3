from django.db import models
import core.models
from enum import Enum


class Driver(models.Model):
<<<<<<< HEAD
    bio = models.CharField(max_length=1024, blank=True, null=True)
    user = models.ForeignKey(core.models.User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
=======
    bio = models.CharField(max_length=1024, blank=True)
    user = models.ForeignKey(core.models.User, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    update_at = models.DateTimeField(auto_now=True, verbose_name='update_at')
>>>>>>> origin/master

    def __str__(self):
        return f"{type(self).__name__}: {self.user}"


class VehicleBrand(models.Model):
    name = models.CharField(max_length=254)
<<<<<<< HEAD
    description = models.CharField(max_length=245, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
=======
    description = models.CharField(max_length=245, blank=True)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    update_at = models.DateTimeField(auto_now=True, verbose_name='update_at')
>>>>>>> origin/master

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class VehicleType(models.Model):
    name = models.CharField(max_length=255)
<<<<<<< HEAD
    description = models.CharField(max_length=245, blank=True, null=True)
    image = models.ForeignKey(core.models.Image, blank=True, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
=======
    description = models.CharField(max_length=245, blank=True)
    image = models.ForeignKey(core.models.Image, blank=True, null=True, on_delete=models.SET_NULL)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    update_at = models.DateTimeField(auto_now=True, verbose_name='update_at')
>>>>>>> origin/master

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class VehicleModel(models.Model):
    name = models.CharField(max_length=254)
    volume = models.DecimalField(decimal_places=5, max_digits=20)
    payload = models.DecimalField(decimal_places=5, max_digits=20)
    type = models.ForeignKey(VehicleType, on_delete=models.DO_NOTHING)
<<<<<<< HEAD
    description = models.CharField(max_length=254, blank=True, null=True)
    brand = models.ForeignKey(VehicleBrand, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
=======
    description = models.CharField(max_length=254, blank=True)
    brand = models.ForeignKey(VehicleBrand, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    update_at = models.DateTimeField(auto_now=True, verbose_name='update_at')
>>>>>>> origin/master

    def __str__(self):
        return f"{type(self).__name__}: {self.name}"


class VehicleStatus(Enum):
    active = 0
    inactive = 1
    broken = 2

    @classmethod
    def values(cls):
<<<<<<< HEAD
        return [(attr.name, attr.value) for attr in cls]
=======
        return [(attr.value, attr.name) for attr in cls]
>>>>>>> origin/master


class Vehicle(models.Model):
    registration_plate = models.CharField(max_length=50)
    driver = models.ForeignKey(Driver, on_delete=models.CASCADE)
    model = models.ForeignKey(VehicleModel, on_delete=models.DO_NOTHING)
    status = models.IntegerField(choices=VehicleStatus.values())
<<<<<<< HEAD
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.model} - {self.registration_plate}"
=======

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    update_at = models.DateTimeField(auto_now=True, verbose_name='update_at')

    def __str__(self):
        return f"{type(self).__name__}: {self.model} {self.registration_plate}"
>>>>>>> origin/master


class VehicleImage(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    image = models.ForeignKey(core.models.Image, on_delete=models.CASCADE)
<<<<<<< HEAD
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='updated_at')
=======

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='created_at')
    update_at = models.DateTimeField(auto_now=True, verbose_name='update_at')
>>>>>>> origin/master
