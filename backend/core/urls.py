from django.urls import include, path
from rest_framework import routers
<<<<<<< HEAD
from .views import UserViewSet, AddressViewSet, CityViewSet, ImageViewSet, MessageViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'address', AddressViewSet)
router.register(r'city', CityViewSet)
router.register(r'image', ImageViewSet)
router.register(r'message', MessageViewSet)

urlpatterns = [
    path('', include(router.urls))
]
=======
from .views import UserViewSet, CityViewSet, MessageViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'cities', CityViewSet)
router.register(r'messages', MessageViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
>>>>>>> origin/master
